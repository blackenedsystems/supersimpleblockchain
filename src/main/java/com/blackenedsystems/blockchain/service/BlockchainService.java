package com.blackenedsystems.blockchain.service;

import com.blackenedsystems.blockchain.model.Block;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BlockchainService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BlockchainService.class);

    private static final String DATA_DIR = "./data";

    private final ObjectMapper objectMapper;

    private File dataDirectory;
    private List<Block> chainData;

    public BlockchainService() throws NoSuchAlgorithmException {
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        chainData = sync();

        LOGGER.info(this.toString());
    }

    public List<Block> getChainData() {
        return chainData;
    }

    private Block createFirstBlock() throws NoSuchAlgorithmException {
        Map<String, Object> blockData = new HashMap<>();
        blockData.put("index", 0L);
        blockData.put("timestamp", LocalDateTime.now());
        blockData.put("data", "First Data Block");
        blockData.put("previousHash", "");
        return new Block(blockData);
    }

    private void saveBlock(Block block) {

        try {
            String jsonString = objectMapper.writeValueAsString(block);
            String fileName = String.format("%s/%06d.json", DATA_DIR, block.getIndex());
            FileWriter fileWriter = new FileWriter(new File(fileName));
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(jsonString);
            bufferedWriter.close();
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Unable to save Block", e);
        } catch (IOException e) {
            throw new RuntimeException("Error writing block to disk", e);
        }

    }

    private List<Block> sync() throws NoSuchAlgorithmException {
        checkBlockchainExists();

        List<Block> blocks = new ArrayList<>();

        for (File file : this.dataDirectory.listFiles()) {
            if (file.getName().endsWith(".json")) {
                try {
                    String jsonBlock = readBlockFile(file);
                    Block block = objectMapper.readValue(jsonBlock, Block.class);
                    blocks.add(block);
                } catch (IOException e) {
                    LOGGER.error("Unable to read file {}", file.getName(), e);
                }
            }
        }
        return blocks;
    }

    private String readBlockFile(File file) throws IOException {
        FileReader fileReader = new FileReader(file);
        StringBuilder buf = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                buf.append(line);
            }
        }
        return buf.toString();
    }

    private void checkBlockchainExists() throws NoSuchAlgorithmException {
        this.dataDirectory = new File(DATA_DIR);
        if (!dataDirectory.exists()) {
            dataDirectory.mkdir();
        }
        if (!dataDirectory.isDirectory()) {
            throw new RuntimeException("Cannot write to blockchain directory: " + DATA_DIR);
        }
        if (dataDirectory.listFiles().length == 0) {
            Block firstBlock = createFirstBlock();
            saveBlock(firstBlock);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("BlockchainService{");
        for (Block block : chainData) {
            sb.append(block.toString());
        }
        sb.append("}");
        return sb.toString();
    }
}
