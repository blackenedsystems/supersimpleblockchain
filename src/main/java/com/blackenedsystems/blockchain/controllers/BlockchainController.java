package com.blackenedsystems.blockchain.controllers;

import com.blackenedsystems.blockchain.service.BlockchainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class BlockchainController {

    private final BlockchainService blockchainService;

    @Autowired
    public BlockchainController(final BlockchainService blockchainService) {
        this.blockchainService = blockchainService;
    }

    @RequestMapping("/show")
    public ModelAndView listBlockChain() {
        ModelAndView modelAndView = new ModelAndView("show");
        modelAndView.addObject("blocks", blockchainService.getChainData());
        return modelAndView;
    }
}
