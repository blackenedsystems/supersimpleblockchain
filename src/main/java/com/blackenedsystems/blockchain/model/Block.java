package com.blackenedsystems.blockchain.model;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.Map;

public class Block {

    private long index;
    private LocalDateTime timestamp;
    private String data;
    private String hash;
    private String previousHash;
    private String nonce = "None";

    public Block() {
    }

    public Block(Map<String, Object> blockData) throws NoSuchAlgorithmException {
        this.index = (Long) blockData.get("index");
        this.timestamp = (LocalDateTime) blockData.get("timestamp");
        this.data = (String) blockData.get("data");

        if (blockData.containsKey("previousHash")) {
            this.previousHash = (String) blockData.get("previousHash");
        }

        if (blockData.containsKey("nonce")) {
            this.nonce = (String) blockData.get("nonce");
        }

        if (blockData.containsKey("hash")) {
            this.hash = (String) blockData.get("hash");
        } else {
            this.hash = createSelfHash();
        }
    }

    private String headerString() {
        return String.valueOf(this.index) + this.previousHash + this.data + this.timestamp + this.nonce;
    }

    private String createSelfHash() throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest(headerString().getBytes(StandardCharsets.UTF_8));
        return new String(hash);
    }

    @Override
    public String toString() {
        return String.format("Block{prevHash: %s, hash: %s}", this.previousHash, this.hash);
    }

    public long getIndex() {
        return index;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public String getData() {
        return data;
    }

    public String getHash() {
        return hash;
    }

    public String getPreviousHash() {
        return previousHash;
    }

    public String getNonce() {
        return nonce;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public void setPreviousHash(String previousHash) {
        this.previousHash = previousHash;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }
}
