# Super Simple Blockchain

Based on a series of blogs titled 'How to Build Your Own Blockchain' published on https://bigishdata.com/.  

1. [Creating, Storing, Syncing, Displaying, Mining, and Proving Work](https://bigishdata.com/2017/10/17/write-your-own-blockchain-part-1-creating-storing-syncing-displaying-mining-and-proving-work/)
2. [Syncing Chains From Different Nodes](https://bigishdata.com/2017/10/27/build-your-own-blockchain-part-2-syncing-chains-from-different-nodes/)
3. [Writing Nodes that Mine and Talk](https://bigishdata.com/2017/11/02/build-your-own-blockchain-part-3-writing-nodes-that-mine/)
4. [Bitcoin Proof of Work Difficulty Explained](https://bigishdata.com/2017/11/13/how-to-build-a-blockchain-part-4-1-bitcoin-proof-of-work-difficulty-explained/)

The code in the articles is written in Python.  To help me understand the concepts I decided to convert the code to Java.  I've tagged the code as I completed the implementation of each part of the blog series.

DISCLAIMER
----------
This code is intended as a learning tool and as such I make no claim as to its correctness or fitness for any purpose other than learning.

You are free to do whatever you want with this code, however, **you do so at your own risk**!  